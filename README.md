Question : Steps to Work on File 

1. Open Github code 
2. Go to the repository link provided.
3. Add the license and the readme files associated.
4. Please add a project of your choice.
5. Ensure correct version of licenses are added.
6. Add the wiki and Issue tracker to your Project.
7. Create an issue that needs enhancement.
8. Please install a reply message that would be deployed to the enhancement.
9. Push and retrieve the project.
10. Add the html file to the necessary page.

License :

Licensing a repository Public repositories on GitHub are often used to share open source software. For your repository to truly be open source, you'll need to license it so that others are free to use, change, and distribute the software.