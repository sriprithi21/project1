/****************************Dependencies****************************/
// import dependencies you will use
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
//What is this programming construct or structure in js?
//Destructuring an object example in tests.js
const { check, validationResult, header } = require("express-validator");
const { json } = require("body-parser");
//get express session
const session = require("express-session");
const fileUpload = require("express-fileupload");

/****************************Database****************************/
//MongoDB
// Takes two arguments path - Includes type of DB, ip with port and name of database
// If awesomestore was not created this would create it through code!!!
mongoose.connect("mongodb://localhost:27017/awesomestore", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

var nameRegex = /^[a-zA-Z0-9]{1,}\s[a-zA-Z0-9]{1,}$/;

/****************************Models****************************/
const Admin = mongoose.model("admin", {
    username: String,
    password: String,
});

// set up the model for the order
const Order = mongoose.model("Order", {
    name: String,
    email: String,
    phone: String,
    postcode: String,
    lunch: String,
    tickets: Number,
    campus: String,
    subTotal: Number,
    tax: Number,
    total: Number,
});

/****************************Variables****************************/
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));

// Setup session to work with app
// secret is a random string to use for the the hashes to save session cookies.
// resave - false prevents really long sessions and security threats from people not logging out.
// saveUninitialized - record a session of a user to see how many users were on your site even if
// they did not login or create any session variables.
myApp.use(
    session({
        secret: "superrandomsecret", //Should look more like 4v2j3h4h4b324b24k2b3jk4b24kj32nb4
        resave: false,
        saveUninitialized: true,
    })
);

//parse application json
myApp.use(express.json());
// set path to public folders and view folders
myApp.set("views", path.join(__dirname, "views"));
//use public folder for CSS etc.
myApp.use(express.static(__dirname + "/public"));
myApp.set("view engine", "ejs");

/****************************Page Routes****************************/
//Edit page
//Use uniques mongodb id
myApp.get("/edit/:postId", function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        var postId = req.params.postId;
        var allPosts;
        Post.find({}).exec(function (err, posts) {
            allPosts = posts;
        });
        Post.findOne({ title: postId }).exec(function (err, post) {
            console.log("Error: " + err);
            console.log("Order: " + post);
            if (post) {
                res.render("editPost", {
                    post: post, 
                    status: ""
                });
            } else {
                console.log("error");
            }
        });
    } else {
        res.redirect("/login");
    }
});

myApp.post(
    "/edit/process2",
    function (req, res) {
        console.log("In Prcoess 2");
        //fetch all the form fields
        var title = req.body.title; // the key here is from the name attribute not the id attribute
        var desc = req.body.desc;
    
        // get the name of the file
        var imageName = req.files.imageName.name;
        // get the actual file (temporary file)
        var imageNameFile = req.files.imageName;
        console.log("🚀 ~ imageNameFile", imageNameFile);
        // decide where to save it (should also check if the file exists and then rename it before saving or employ some logic to come up with unique file names)
        var imageNamePath = "public/post/" + imageName;
        // move temp file to the correct folder (public folder)
        imageNameFile.mv(imageNamePath, function (err) {
            console.log("error", err);
        });
    
        // create an object with the fetched data to send to the view
        var pageData = {
            desc: desc,
            imageName: imageName,
        };
    
        console.log("🚀 ~ pageData", pageData);
    
        Post.findOneAndUpdate({title: title}, pageData).exec((err, doc)=> console.log(doc));
        // Post.findOneAndUpdate({title: title}, pageData).then(() => res.render("adminEditPost", {status: "success"}));
        res.render("editPost", {status: "success"})
    }
);

//Delete page
//Use uniques mongodb id
myApp.get("/delete/:postTitle", function (req, res) {
    console.log("deleting post", req.params.postTitle);
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        //delete
        var postTitle = req.params.postTitle;
        console.log("🚀 ~ postTitle", postTitle);
        Post.findOneAndDelete({ title: postTitle }).exec(function (err, post) {
            console.log("Error: " + err);
            if (post) {
                res.render("delete", {
                    message: "Successfully deleted!",
                });
            } else {
                res.render("delete", {
                    message: "Sorry, could not delete!",
                });
            }
        });
        // Order.findByIdAndDelete({ _id: orderid }).exec(function (err, order) {
        //     console.log("Error: " + err);
        //     console.log("Order: " + order);
        //     if (order) {
        //         res.render("delete", {
        //             message: "Successfully deleted!",
        //             userLoggedIn: req.session.userLoggedIn,
        //         });
        //     } else {
        //         res.render("delete", {
        //             message: "Sorry, could not delete!",
        //             userLoggedIn: req.session.userLoggedIn,
        //         });
        //     }
        // });
    } else {
        res.redirect("/login");
    }
});

// jai
myApp.get("/edit", function (req, res) {
    console.log("in edit all posts page");
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        var allPosts;
        var customPosts = [];
        Post.find({}).exec(function (err, posts) {
            allPosts = [...posts];
            allPosts.forEach(function (element) {
                var tempObject = { ...element._doc };
                tempObject.deleteLink = "/delete/" + tempObject.title;
                tempObject.editLink = "/edit/" + tempObject.title;
                customPosts.push(tempObject);
            });

            res.render("editPages", { posts: customPosts });
        });
    } else {
        res.redirect("/login");
    }
});

// jai all posts homepage
myApp.get("/post/:postid", function (req, res) {
    console.log("in get post");
    // check if the user is logged in
    // if (req.session.userLoggedIn) {
        //delete
        var postid = req.params.postid;
        console.log(postid);
        var allPosts;
        Post.find({}).exec(function (err, posts) {
            allPosts = posts;
        });
        Post.findOne({ title: postid }).exec(function (err, post) {
            console.log("Error: " + err);
            console.log("Order: " + post);
            if (post) {
                res.render("appPost", {
                    post: post,
                    posts: allPosts,
                });
            } else {
                console.log("error");
            }
        });
    // } else {
    //     res.redirect("/login");
    // }
});

//Login Page
myApp.get("/login", function (req, res) {
    console.log("In Login get");
    res.render("login", { userLoggedIn: req.session.userLoggedIn });
});

//Write some validations in the post for both username and password
// Sessions variables are used across the web site so that you do not have to pass
// variables from page to page. Session variables can be use to store more than just login information
// and can be used to store all products in a cart for example if the products expanded across
// multiple pages.
myApp.post("/login", function (req, res) {
    console.log("In Login post");
    var user = req.body.username;
    var pass = req.body.password;

    //console.log(username);
    //console.log(password);

    Admin.findOne({ username: user, password: pass }).exec(function (
        err,
        admin
    ) {
        // log any errors
        console.log("Error: " + err);
        console.log("Admin: " + admin);
        if (admin) {
            //store username in session and set logged in true
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            // redirect to the dashboard
            res.redirect("/adminDashboard");
            // res.redirect("/allorders");
        } else {
            res.render("login", { error: "Sorry, cannot login!" });
        }
    });
});

myApp.post("/process2", function (req, res) {
    console.log("In Prcoess 2");
    //fetch all the form fields
    var title = req.body.title; // the key here is from the name attribute not the id attribute
    console.log("🚀 ~ title", title);
    var desc = req.body.desc;
    console.log("🚀 ~ desc", desc);

    // get the name of the file
    var imageName = req.files.imageName.name;
    // get the actual file (temporary file)
    var imageNameFile = req.files.imageName;
    console.log("🚀 ~ imageNameFile", imageNameFile);
    // decide where to save it (should also check if the file exists and then rename it before saving or employ some logic to come up with unique file names)
    var imageNamePath = "public/post/" + imageName;
    // move temp file to the correct folder (public folder)
    imageNameFile.mv(imageNamePath, function (err) {
        console.log("error", err);
    });

    // create an object with the fetched data to send to the view
    var pageData = {
        // title: title,
        desc: desc,
        imageName: imageName,
        // path: `/post/${title}`,
    };

    console.log("🚀 ~ pageData", pageData);

    Post.findOneAndUpdate({title: title}, pageData).then(() => 
        res.render("editPost", {status: success})
    )
    // save data to database
    // var myPost = new Post(pageData); // not correct yet, we need to fix it.

    // myPost.save().then(() => console.log("Post Created"));

    // send the data to result view and render it
    // res.render("card", pageData);
    // res.render("adminAddPage", { status: "success", imageName: imageName });
});

myApp.post("/process", function (req, res) {
    //fetch all the form fields
    var title = req.body.title; // the key here is from the name attribute not the id attribute
    console.log("🚀 ~ title", title);
    var desc = req.body.desc;
    console.log("🚀 ~ desc", desc);

    // get the name of the file
    var imageName = req.files.imageName.name;
    // get the actual file (temporary file)
    var imageNameFile = req.files.imageName;
    console.log("🚀 ~ imageNameFile", imageNameFile);
    // decide where to save it (should also check if the file exists and then rename it before saving or employ some logic to come up with unique file names)
    var imageNamePath = "public/post/" + imageName;
    // move temp file to the correct folder (public folder)
    imageNameFile.mv(imageNamePath, function (err) {
        console.log("error", err);
    });

    // create an object with the fetched data to send to the view
    var pageData = {
        title: title,
        desc: desc,
        imageName: imageName,
        path: `/post/${title}`,
    };

    console.log("🚀 ~ pageData", pageData);

    // save data to database
    var myPost = new Post(pageData); // not correct yet, we need to fix it.

    myPost.save().then(() => console.log("Post Created"));

    // send the data to result view and render it
    // res.render("card", pageData);
    res.render("addOrderPage", { status: "success", imageName: imageName });
});

myApp.get("/adminDashboard", function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        console.log(req.session.userLoggedIn);
        res.render("dashBoard");
    } else {
        // otherwise send the user to the login page
        res.redirect("/login");
    }
});

myApp.get("/adminAddPage", function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        console.log(req.session.userLoggedIn);
        res.render("addOrderPage", { status: "", imageName: "" });
    } else {
        // otherwise send the user to the login page
        res.redirect("/login");
    }
});

//All orders page
//fetch all. If there is an error it will put it in the err variable otherwise the orders
//will be returned in the orders variable.
// All orders page
myApp.get("/allorders", function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        console.log(req.session.userLoggedIn);
        Order.find({}).exec(function (err, orders) {
            res.render("allorders", {
                orders: orders,
                userLoggedIn: req.session.userLoggedIn,
            });
        });
    } else {
        // otherwise send the user to the login page
        res.redirect("/login");
    }
});

//Logout Page
myApp.get("/logout", function (req, res) {
    //Remove variables from session
    req.session.username = "";
    req.session.userLoggedIn = false;
    //res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render("login", { error: "Logged Out" });
});

//home page
myApp.get("/", function (req, res) {
    res.render("form", { userLoggedIn: req.session.userLoggedIn }); // no need to add .ejs to the file name
});

myApp.post(
    "/",
    [
        check("name", "Name is required!").notEmpty(),
        check("email", "Email is required").isEmail(),
        check("lunch", "").custom(customLuchTicketsValidation),
    ],
    function (req, res) {
        const errors = validationResult(req);
        console.log(errors); //logging this error will show us in the terminal that errors is an array and msg is what we need to print client side
        if (!errors.isEmpty()) {
            res.render("form", {
                errors: errors.array(),
            });
        } else {
            var name = req.body.name;
            var email = req.body.email;
            var phone = req.body.phone;
            var postcode = req.body.postcode;
            var tickets = req.body.tickets;
            var lunch = req.body.lunch;
            var campus = req.body.campus;

            var subTotal = tickets * 20;
            if (lunch == "yes") {
                subTotal += 15;
            }
            var tax = subTotal * 0.13;
            var total = subTotal + tax;

            var pageData = {
                name: name,
                email: email,
                phone: phone,
                postcode: postcode,
                lunch: lunch,
                tickets: tickets,
                campus: campus,
                subTotal: subTotal,
                tax: tax,
                total: total,
            };

            var myNewOrder = new Order(pageData);
            myNewOrder.save().then(() => console.log("New order saved"));

            res.render("form", pageData);
        }
    }
);

/****************************Validation Functions****************************/
// phone regex for 123-123-2341
var phoneRegex = /^[0-9]{3}\-?[0-9]{3}\-?[0-9]{4}$/;
// positive number regex
var positiveNumberRegex = /^[1-9][0-9]*$/;

/* Custom validation for tickets and lunch
    1. Tickets should be a number.
    2. User has to buy at least 3 tickets or have lunch 
*/
function customLuchTicketsValidation(value, { req }) {
    var tickets = req.body.tickets;
    if (Math.sign(parseInt(tickets)) != 1) {
        throw new Error("Please select number of tickets");
    }
    // if(!checkRegex(tickets, positiveNumberRegex)){
    //     throw new Error('Please select number of tickets');
    // }
    else {
        tickets = parseInt(tickets);
        if (tickets < 3 && value != "yes") {
            throw new Error(
                "Lunch must be ordered if less than 3 tickets are bought."
            );
        }
    }
    return true;
}

//Function to check a string using regex
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    return false;
}

function customTestValidation(value) {
    throw new Error("This is an error");
}

// Custom validation functions return true if conditions are satisfied or throws an error of type Error
// Custom phone validation
function customPhoneValidation(value) {
    console.log(json.toString(value));
    console.log(value);
    console.log(value[0]);
    console.log(value[1]);

    if (!checkRegex(value, phoneRegex)) {
        throw new Error("Phone should be in the format (xxx)-xxx-xxxx");
    }
    return true;
}

// start the server and listen at a port
myApp.listen(1234);

//tell everything was ok
console.log("Everything executed fine.. website at port 80812...");
